#!/bin/bash
NAME='Sony PLAYSTATION(R)3 Controller'

ids=$(xinput list | grep  "$NAME" | grep -o -e "id=.." | xargs | sed "s/id=//g")
for id in $ids; do
    echo "Disabling Mouse/Key events for ID $id"
    xinput set-prop $id "Device Enabled" 0
done