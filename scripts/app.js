/**
 * @fileOverview CV Game is a small retro-style 2D action game. The goal is to control the main character
 *               who has to shoot wild rabbits using a flamer gun in order to collect fragments of my personal CV
 *               hidden in treasure chests and thus unveil my profesional skills and experiences.
 * @author Romain Joly <contact@romain-joly.com>
 * @version 1.0.0
 */
(function(w, d, undefined) {
  'use strict';

  /**
   * @see {@link http://paulirish.com/2011/requestanimationframe-for-smart-animating/}
   * @see {@link http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating}
   * requestAnimationFrame polyfill by Erik Möller
   * fixes from Paul Irish and Tino Zijdel
   */
  (function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !w.requestAnimationFrame; ++x) {
      w.requestAnimFrame = w[vendors[x] + 'RequestAnimationFrame'];
      w.cancelAnimFrame = w[vendors[x] + 'CancelAnimationFrame'] || w[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!w.requestAnimFrame) {
      w.requestAnimFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = w.setTimeout(function() {
          callback(currTime + timeToCall);
        },
        timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };
    }

    if (!w.cancelAnimFrame) {
      w.cancelAnimFrame = function(id) {
        clearTimeout(id);
      };
    }
  }());
  
  /**
   * @see {@link https://gist.github.com/devongovett/1381839}
   * Minimal classList shim for IE 9
   * By Devon Govett
   * MIT LICENSE
   */
  (function() {
    if (!("classList" in d.documentElement) && Object.defineProperty && typeof HTMLElement !== 'undefined') {
      Object.defineProperty(HTMLElement.prototype, 'classList', {
        get: function() {
          var self = this;
          function update(fn) {
            return function(value) {
              var classes = self.className.split(/\s+/),
              index = classes.indexOf(value);
              fn(classes, index, value);
              self.className = classes.join(" ");
            }
          }
          var ret = {                    
            add: update(function(classes, index, value) {
              ~index || classes.push(value);
            }),
            remove: update(function(classes, index) {
              ~index && classes.splice(index, 1);
            }),
            toggle: update(function(classes, index, value) {
              ~index ? classes.splice(index, 1) : classes.push(value);
            }),
            contains: function(value) {
              return !!~self.className.split(/\s+/).indexOf(value);
            },
            item: function(i) {
              return self.className.split(/\s+/)[i] || null;
            }
          };
          Object.defineProperty(ret, 'length', {
            get: function() {
              return self.className.split(/\s+/).length;
            }
          });
          return ret;
        }
      });
    }
  }());
  
  /***********************************************************************************************/
  /*                                        Utils module                                         */
  /* @module Utils                                                                               */
  /* @desc Provides a set of tools for game mechanics (positioning, drawing, collisions, etc...) */
  /***********************************************************************************************/
  var Utils = (function() {
      /**
       * Utils object
       * @constructor
       * @this {Utils}
       */
      function Utils() { }
      
      /**
       * Checks if current browser is a mobile browser
       * @see {@link https://developer.mozilla.org/fr/docs/Web/HTTP/Detection_du_navigateur_en_utilisant_le_user_agent}
       * @returns {boolean}
       */
      Utils.isMobileDevice = function() {
        return w.navigator.userAgent.match(/mobi/i);
      };
      
      /**
       * Get a random integer in range [min, max]
       * @param {number} min - minimum range limit
       * @param {number} max - maximum range limit
       * @returns {number} a random number in the range
       */ 
      Utils.getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      };

      /**
       * Get a random x or y coordinate based on container width or height and element allowed interval
       * @param {number} a - container width or height
       * @param {number} b - minimum allowed interval between elements of same type
       * @param {number} c - minimum allowed interval between elements of same type
       * @returns {number} a random coordinate
       */ 
      Utils.getRandomPos = function(a, b, c) {
        return Math.floor(Math.random() * a / b) * c;
      };

      /**
       * Create element with given type and attributes
       * @param {string} type - element type
       * @param {Object} attributes - element attributes
       * @returns {HTMLElement}
       */
      Utils.createElement = function(type, attributes) {
        var element = d.createElement(type);
        Object.keys(attributes).forEach(function(key) {
          element.setAttribute(key, attributes[key]);
        });
        return element;
      };

      /**
       * Init item position with given coordinates
       * @param {Object} item - the item to be positioned
       * @param {number} x - horizontal coordinate
       * @param {number} y - vertical coordinate
       * @returns {Object}
       */
      Utils.initPosition = function(item, x, y) {
        var position = {x: x, y: y, width: item.element.style.width, height: item.element.style.height};
        item.element.style.left = x + 'px';
        item.element.style.top = y + 'px';
        return position;
      };

      /**
       * Get the distance between two items
       * @param {Object} item1
       * @param {Object} item2
       * @returns {number}
       */
      Utils.getDistance = function(item1, item2) {
        var pos1 = (item1 instanceof HTMLDivElement) ? item1.getBoundingClientRect() : item1;
        var pos2 = (item2 instanceof HTMLDivElement) ? item2.getBoundingClientRect() : item2;
        var deltaX = pos1.left - pos2.left;
        var deltaY = pos1.top - pos2.top;
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
      };

      /**
       * Draw item at current position
       * @param {Object} item
       */
      Utils.draw = function(item) {
        item.element.style.left = !isNaN(item.position.x) ? item.position.x.toString() + 'px' : item.element.style.left;
        item.element.style.top = !isNaN(item.position.y) ? item.position.y.toString() + 'px' : item.element.style.top;
      };

      /**
       * Simple collision check
       * @param {Object} rect1
       * @param {Object} rect2
       * @returns {boolean}
       */
      Utils.isColliding = function(rect1, rect2) {
        return (rect1.left < rect2.left + rect2.width
          && rect1.left + rect1.width > rect2.left
          && rect1.top < rect2.top + rect2.height
          && rect1.height + rect1.top > rect2.top);
      };

      /**
       * Check if an item is colliding with multiple items
       * @param {Object} item
       * @param {Object[]} itemCollection
       * @returns {boolean}
       */
      Utils.isCollidingMultiple = function(item, itemCollection) {
        for (var i in itemCollection) {
          if (itemCollection[i] === item) {
            continue;
          }
          if (Utils.isColliding(item, itemCollection[i])) {
            return true;
          }
        }
        return false;
      };

      /**
       * Populate map with items at random positions not overlapping other items of a given class
       * @param {Object} itemsType - item to populate class name
       * @param {number} minItems - the minimum number of items of a given type to generate
       * @param {number} maxItems - the maximum number of items of a given type to generate
       * @param {HTMLElement} map - the container element where items are populated
       * @param {number} intervalA - minimum allowed interval between items of same type
       * @param {number} intervalB - minimum allowed interval between items of same type
       * @param {string} notOverlappingClass - forbids positioning current item over items of a given class
       * @param {number} minDistanceFromCharacter - minimum allowed item distance from the character (to give enough room to the character at game startup)
       * @param {number} itemCounter - allows to keep track of the number of items already created in case we need to create new ones
       * @returns {Object[]} items
       */
      Utils.populateMapWithObjects = function(itemsType, minItems, maxItems, map, intervalA, intervalB, notOverlappingClass, minDistanceFromCharacter, itemCounter) {
        var items = [];
        var item, randomX, randomY, area;
        // retrieve coordinates of items already positioned on the map
        var existingItemsAreas = Array.prototype.map.call(d.querySelectorAll(notOverlappingClass), function(obj) {
          var rect = (obj instanceof HTMLDivElement) ? obj.getBoundingClientRect() : obj;
          return rect;
        });
        var mapArea = (map instanceof HTMLDivElement) ? map.getBoundingClientRect() : map;
        var currentCounter = itemCounter || 0;
        // iterate over items to be positioned
        for (var i = currentCounter; i < Utils.getRandomInt(minItems, maxItems) + currentCounter; i++) {
          item = new itemsType(i);
          if (itemCounter) {
            item.element.classList.add('new');
          }
          // append item to the map
          items[item.element.id] = item;
          map.appendChild(item.element);

          // iterate while we don't find a suitable position for the newly created item:
          // item is either colliding with another one or is too close from the character
          do {
            randomX = Utils.getRandomPos(mapArea.width, intervalA, intervalB); // get random horizontal coordinate
            randomY = Utils.getRandomPos(mapArea.height, intervalA, intervalB); // get random vertical coordinate
            area = {left: mapArea.left + randomX, top: mapArea.top + randomY, width: item.element.clientWidth, height: item.element.clientHeight}; // area to check
          } while(Utils.isCollidingMultiple(area, existingItemsAreas)
                    || Utils.getDistance(area, d.getElementById('character_hitbox')) < minDistanceFromCharacter);
          
          // if area ok, push it to existing item areas for next check and init current item position with coordinates
          existingItemsAreas.push(area);
          item.position = Utils.initPosition(item, randomX, randomY);
        }

        return items;
      };

      /**
       * Check potential collisions between a moving hitbox and a set of items
       * @param {HTMLDivElement} hitbox - the current div element to check collisions against
       * @param {HTMLDivElement[]} items - a collection of div elements to check collisions against
       * @param {number} moveX - the intended horizontal move
       * @param {number} moveY - the intended vertical move
       * @returns {Object} x - a boolean to check horizontal collision
       *                   y - a boolean to check vertical collision
       *                   collidingIDs - an array of boolean to check collision by id of colliding items
       */
      Utils.hasCollisions = function(hitbox, items, moveX, moveY) {
        var pos1 = (hitbox instanceof HTMLDivElement) ? hitbox.getBoundingClientRect() : hitbox;
        var pos2;
        var hasCollisionX = false;
        var hasCollisionY = false;
        var collidingIDs = [];

        for (var i in items) {
          // skip if current item is hitbox
          if (items[i] === hitbox) {
            continue;
          }

          pos2 = (items[i] instanceof HTMLDivElement) ? items[i].getBoundingClientRect() : items[i];

          // check horizontal collision
          if (pos1.right + moveX >= pos2.left
                && pos1.left + moveX <= pos2.right
                && pos1.bottom >= pos2.top
                && pos1.top <= pos2.bottom) {
            hasCollisionX = true;
          }

          // check vertical collision
          if (pos1.right >= pos2.left
                && pos1.left <= pos2.right
                && pos1.bottom + moveY >= pos2.top
                && pos1.top + moveY <= pos2.bottom) {
            hasCollisionY = true;
          }

          // store colliding div ids
          if (items[i].id !== '') {
            if (hasCollisionX || hasCollisionY) {
              collidingIDs[items[i].id] = true;
            } else {
              collidingIDs[items[i].id] = false;
            }
          }
        }
        Game.debug && console.log(hasCollisionX, hasCollisionY, collidingIDs);
        return {
          x:            hasCollisionX,
          y:            hasCollisionY,
          collidingIDs: collidingIDs
        };
      };

      /**
       * Check if moving item is colliding with parent container box
       * @param {HTMLDivElement} item - element to check
       * @param {HTMLDivElement} box - container element
       * @param {number} moveX - the intended horizontal move
       * @param {number} moveY - the intended vertical move
       * @returns {boolean}
       */
      Utils.hasInnerBoxCollision = function(item, box, moveX, moveY) {
        var itemPos = (item instanceof HTMLDivElement) ? item.getBoundingClientRect() : item;
        var boxPos = (box instanceof HTMLDivElement) ? box.getBoundingClientRect() : box;
        return itemPos.left + moveX <= boxPos.left
          || itemPos.top + moveY <= boxPos.top 
          || itemPos.left + moveX >= boxPos.left + boxPos.width - itemPos.width
          || itemPos.top + moveY >= boxPos.top + boxPos.height - itemPos.height;
      };

      /**
       * Play audio file in loop
       * @param {Audio} audio - the audio file
       */
      Utils.playAudioLoop = function(audio) {
        if (typeof audio.loop === 'boolean') {
          audio.loop = true;
        } else { // fallback for browsers not supporting audio.loop
          audio.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
          }, false);
        }
        audio.play();
      };

      /**
       * Merge two objects together
       * @param {Object} obj1
       * @param {Object} obj2
       * @returns {Object}
       */
      Utils.mergeObjects = function(obj1, obj2) {
        var newObj = {};
        for (var i in obj1) {
          if (Object.prototype.hasOwnProperty.call(obj1, i)) {
            newObj[i] = obj1[i];
          }
        }
        for (var j in obj2) {
          if (Object.prototype.hasOwnProperty.call(obj2, j)) {
            newObj[j] = obj2[j];
          }
        }

        return newObj;
      };

      return Utils;
    }()),
    /**************************************************************/
    /*                          Data module                       */
    /* @module Data                                               */
    /* @desc Stores the CV data that is hidden in treasure chests */
    /**************************************************************/
    Data = {
      score_0: {
        score:   0, 
        message: '<img src="images/sprites/photo.png" alt="Photo Romain Joly"><p>Bienvenue sur le jeu CV de Romain Joly, développeur JS.</p><p>Pour démarrer le jeu, appuyez sur la touche <strong>ENTRER</strong>.</p><p>Puis utilisez le pavé numérique (touches <strong>1 à 9</strong>) ou les touches directionnelles <strong>^ ` < ></strong> pour déplacer le personnage, et la barre d\'espace pour éliminer les lapins sauvages et récupérer les pièces de mon CV cachées dans les coffres disséminés sur la carte.</p>'
      },
      score_5: {
        score:   5,
        message: '<h2>Compétences</h2><ul><li class="js">JavaScript (ES5 / ES6)</li><li class="ng">Angular</li><li class="node">Node (Express, Meteor)</li><li class="php">PHP 7</li><li class="mysql">MySQL</li><li class="mongo">MongoDB</li></ul><ul><li class="jquery">jQuery</li><li class="bootstrap">Bootstrap</li><li class="html">HTML 5</li><li class="css">CSS 3</li><li class="git">Git</li><li class="photoshop">Photoshop</li></ul>'
      },
      score_15: {
        score:   15,
        message: '<h2>Expériences</h2><ul><li class="senzo">Agence Senzo - dév freelance - (2017 - 2018)</li><li class="habiteo">Habiteo - lead dév équipe B2C - (2017)</li><li class="nexway">Nexway - dév PHP sénior - (2014 - 2016)</li><li class="netvendeur">Netvendeur - dév freelance - (2014)</li><li class="solocal">Solocal - dév PHP sénior - (2011 - 2013)</li><li class="sopra">Sopra Group - dév PHP junior - (2007 - 2011)</li><li class="datavance">Datavance - dév PHP junior - (2007)</li></ul>'
      },
      score_30: {
        score:   30,
        message: '<h2>Formations</h2><ul><li class="ifocop">IFOCOP Dév JS (2018)</li><li class="teesside">Master Multimédia (2006)</li><li class="teesside">Licence IBIT (2005)</li><li class="iut">DUT Informatique (2004)</li><li>Bac S (2002)</li></ul>'
      },
      score_50: {
        score:   50,
        message: '<h2>Intérêts</h2><ul><li class="guitar">Guitare</li><li class="running">Course à pied</li><li class="travel">Voyages</li></ul>'
      },
      score_80: {
        score:   80,
        message: '<h2>Contact</h2><ul><li class="address">Adresse: 11 rue Saint-Ambroise, 75011 Paris</li><li class="email">E-mail: <a href="mailto:contact@romain-joly.com">contact@romain-joly.com</a></li><li class="phone">Téléphone: 06-19-75-45-00</li><li class="linkedin">LinkedIn: <a href="https://www.linkedin.com/in/romain-joly-b7786471/" target="_blank">https://www.linkedin.com/in/romain-joly-b7786471/</a></li><li class="gitlab">GitLab: <a href="https://gitlab.com/elazul51" target="_blank">https://gitlab.com/elazul51</a></li></ul>'
      }
    },
    /*************************************************/
    /*                   Game module                 */
    /* @module Game                                  */
    /* @desc Contains the game mechanics and classes */
    /*************************************************/
    Game = (function(w, d, Utils, Data) {
      Game.debug = false;
      Game.nbMinItems = 50; // the minimum number of background items to populate
      Game.nbMaxItems = 100; // the maximum number of background items to populate
      Game.nbMinTrees = 7; // the minimum number of trees to populate
      Game.nbMaxTrees = 10; // the maximum number of trees to populate
      Game.nbMinEnemies = 10; // the minimum number of enemies to populate
      Game.nbMaxEnemies = 15; // the maximum number of enemies to populate
      Game.viewport = d.getElementById('viewport');
      Game.viewportWidth = Game.viewport.clientWidth;
      Game.viewportHeight = Game.viewport.clientHeight;

      /**
      * Game object
      * @constructor
      * @this {Game}
      */
      function Game() {
        this.init();
      }

      /**
       * Init the game elements and properties
       */
      Game.prototype.init = function() {
        this.zoom = 1; // the initial zoom value
        this.turnedOn = false; // check if the game is turned on (if the user has clicked on the TV START button)
        this.started = false; // check if the game is started (if the user has pressed ENTER on title screen)
        this.paused = true; // check if the game is paused (true at startup)
        this.loop = this.loop.bind(this);
        this.keydownHandler = this.keydownHandler.bind(this);
        this.keyupHandler = this.keyupHandler.bind(this);
        this.menuZoomInHandler = this.menuZoomInHandler.bind(this);
        this.menuZoomOutHandler = this.menuZoomOutHandler.bind(this);
        this.menuMusicPlayHandler = this.menuMusicPlayHandler.bind(this);
        this.menuStatsDisplayHandler = this.menuStatsDisplayHandler.bind(this);
        this.menuCrtEffectHandler = this.menuCrtEffectHandler.bind(this);
        this.buttonRepeat = {}; // store ids of repeated buttons when using gamepad
        this.initGamepadListeners();
        this.initSound();
        this.initInput();
        this.initMenu();
        this.initInfoBox();
        this.initCharacter();
        this.initBackgroundItems();
        this.initTrees();
        this.initEnemies();
        this.initStats();
        this.pauseAnimations();
      };

      /**
       * Static method to run the application
       */
      Game.run = function() {
        var game = new this();
        if (w.restart) {
          setTimeout(function() { game.boostrap(game, w.restart) }, 2000);
        } else {
          // when all page content is loaded, we can remove the loading screen and launch the game interface
          w.addEventListener('load', function() {
            w.restart = true; // allows relaunching the game after gameover
            d.getElementById('loading-container').classList.add('fadeout');
            setTimeout(function() { game.boostrap(game) }, 2000);
          }.bind(game));
        }
      };

      /**
       * Bootstrap the game features when user is about to start the game
       * @param {Game} game 
       * @param {boolean} restart 
       */
      Game.prototype.boostrap = function(game, restart) {
        if (restart) {
          game.boostrapAction(game);
        } else {
          d.getElementById('loading-container').style.display = 'none';
          d.getElementById('switch').addEventListener('click', game.boostrapAction.bind(game, game));
        }
      };

      /**
       * Miscellaneous bootstrap actions
       * @param {Game} game 
       */
      Game.prototype.boostrapAction = function(game) {
        // hide game viewport at startup
        Game.viewport.className = '';
        Game.viewport.style.visibility = 'hidden';
        // remove CRT screen noise effect
        d.getElementById('screen').classList.remove('noise');
        // display title screen
        var titleScreen = d.getElementById('title_screen');
        titleScreen.style.display = 'block';
        titleScreen.classList.add('turn_on');
        // remove START button switch
        var tvSwitch = d.getElementById('switch');
        tvSwitch.classList.add('fadeout');
        setTimeout(function() {
          tvSwitch.classList.remove('fadeout');
          tvSwitch.style.display = 'none';
        }, 2000);
        tvSwitch.removeEventListener('click', game.boostrapAction.bind(game, game));
        // start title screen music
        if (!game.sound.intro.muted) {
          Utils.playAudioLoop(game.sound.intro);
        }
        // set turnedOn to true
        game.turnedOn = true;
        // start main game loop
        game.loop();
      };

      /**
       * Start the game after title screen
       */
      Game.prototype.start = function() {
        this.started = true;
        // remove title screen
        d.getElementById('title_screen').style.display = 'none';
        // show game viewport
        Game.viewport.style.visibility = 'visible';
        // stop title screen music
        this.sound.intro.pause();
        this.sound.intro.currentTime = 0;
        // start game music
        if (this.sound.intro.muted) {
          this.sound.music.muted = true;
        } else {
          Utils.playAudioLoop(this.sound.music);
        }
      };

      /**
       * Main game loop allowing to listen to inputs, to update game state and to render graphics
       * @param {DOMHighResTimeStamp} timer - a timestamp that is returned by requestAnimationFrame
       */
      Game.prototype.loop = function(timer) {
        // store the loop timer for further use
        Game.timer = timer;

        // input management (keyboard or gamepad)
        this.pollGamepads();
        if (this.gamepad && this.gamepad.connected) {
          this.gamepadActions();
        } else if (!this.paused && this.started) {
          this.keyActions();
        }
        
        if (!this.paused && this.started) {
          // update game state
          this.update();
          // render graphics
          this.render();
        }
        
        // recursive call to game loop through requestAnimationFrame method
        this.rAF = w.requestAnimFrame(this.loop);
      };

      /**
       * Update the game elements state
       */
      Game.prototype.update = function() {
        // check if game is in gameover state
        if (this.character.HP <= 0) {
          this.gameOver(this.character);
        }

        // change enemies state and positions
        for (var i in Game.enemies) {
          if (Object.prototype.hasOwnProperty.call(Game.enemies, i)) {
            var enemy = Game.enemies[i];
            enemy.move(this.character);
          }
        }

        // change fireballs state and positions
        for (var j in Character.fireballs) {
          if (Object.prototype.hasOwnProperty.call(Character.fireballs, j)) {
            Character.fireballs[j].move();
          }
        }
      };

      /**
       * Render the game elements
       */
      Game.prototype.render = function() {
        // draw character at current position
        Utils.draw(this.character);


        // draw enemies at current positions
        for (var i in Game.enemies) {
          if (Object.prototype.hasOwnProperty.call(Game.enemies, i)) {
            Utils.draw(Game.enemies[i]);
          }
        }

        // draw character at current position
        for (var j in Character.fireballs) {
          if (Object.prototype.hasOwnProperty.call(Character.fireballs, j)) {
            Utils.draw(Character.fireballs[j]);
          }
        }

        // draw current stats
        this.stats.draw(this.character.HP, this.score);
      };

      /**
       * Keydown handler method for keydown event
       * @param {Object} e - event object
       */
      Game.prototype.keydownHandler = function(e) {
        e.preventDefault();
        var k = e.key || e.which || e.keyCode;
        this.keyRepeat = this.keyState[k] ? true : false;
        this.keyState[k] = true;
        // if ENTER key pressed and game is not started, start the game else if game already started and paused, resume the game
        if (this.turnedOn && !this.started && !this.keyRepeat && (this.keyState['Enter'] || this.keyState[13])) {
          this.keyState = {};
          this.start();
        } else if (this.started && this.paused && !(this.gamepad && this.gamepad.connected) && !this.keyRepeat && (this.keyState['Enter'] || this.keyState[13])) {
          this.keyState = {};
          this.resume();
        }
      };

      /**
       * Keyup handler method for keyup event
       * @param {Object} e - event object
       */
      Game.prototype.keyupHandler = function(e) {
        e.preventDefault();
        this.keyRepeat = false;
        var k = e.key || e.which || e.keyCode;
        this.keyState[k] = false;
        // stop character animation on key release
        this.character.stop();
      };

      /**
       * Gamepad connected handler method for gamepad connected event
       */
      Game.prototype.gamepadConnectedHandler = function() {
        d.getElementById('controller').classList.add('connected');
      };

      /**
       * Gamepad disconnected handler method for gamepad disconnected event
       */
      Game.prototype.gamepadDisconnectedHandler = function() {
        d.getElementById('controller').classList.remove('connected');
      };

      /**
       * Init the gamepadconnected and gamepaddisconnected event listeners
       */
      Game.prototype.initGamepadListeners = function() {
        w.addEventListener('gamepadconnected', this.gamepadConnectedHandler, false);
        w.addEventListener('gamepaddisconnected', this.gamepadDisconnectedHandler, false);
      };

      /**
       * Poll Gamepad API to find and connect a standard gamepad
       * @see {@link https://developer.mozilla.org/fr/docs/Web/Guide/API/Gamepad}
       * @see {@link https://w3c.github.io/gamepad/}
       */
      Game.prototype.pollGamepads = function() {
        // look for connected gamepads
        var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
        this.gamepad = null;
        // iterate through connected gamepads and assign the first one that matches with the standard gamepad mapping
        for (var i in gamepads) {
          if (gamepads[i] !== null) {
            if (gamepads[i].mapping === 'standard') {
              d.getElementById('controller').classList.add('connected'); // the gamepad picture lights up
              this.gamepad = gamepads[i];
              break;
            } else if (gamepads[i].id !== undefined) {
              console.log(gamepads[i].id + ' is not supported');
            }
          }
        }
      };

      /**
       * Init game audio files and sound levels
       */
      Game.prototype.initSound = function() {
        this.sound = {
          intro:    new Audio('audio/intro.mp3'),
          music:    new Audio('audio/music.mp3'),
          gameover: new Audio('audio/gameover.wav')
        };
        this.sound.intro.volume = 0.3;
        this.sound.music.volume = 0.3;
      };

      /**
       * Init keyboard input with keydown and keyup event listeners
       */
      Game.prototype.initInput = function() {
        this.keyState = {};
        this.keyRepeat = false;
        d.addEventListener('keydown', this.keydownHandler, false);
        d.addEventListener('keyup', this.keyupHandler, false);
      };

      /**
       * Menu zoom in handler method
       * @param {Object} e - event object
       */
      Game.prototype.menuZoomInHandler = function(e) {
        if (this.zoom === 1.5) {
          return;
        }
        d.getElementById('zoom_out').classList.remove('enabled');
        e.target.classList.add('enabled');
        this.zoom += this.zoom < 1.5 ? 0.05 : 0; // increments zoom size
        d.body.style.transform = 'scale(' + this.zoom + ')'; // CSS zoom property is not compatible with Firefox so we use transform scale instead
      };

      /**
       * Menu zoom out handler method
       * @param {Object} e - event object
       */
      Game.prototype.menuZoomOutHandler = function(e) {
        if (this.zoom === 1) {
          return;
        }
        d.getElementById('zoom_in').classList.remove('enabled');
        e.target.classList.add('enabled');
        this.zoom -= this.zoom > 1 ? 0.05 : 0; // decrements zoom size
        d.body.style.transform = 'scale(' + this.zoom + ')'; // CSS zoom property is not compatible with Firefox so we use transform scale instead
      };

      /**
       * Menu music play handler method
       * @param {Object} e - event object
       */
      Game.prototype.menuMusicPlayHandler = function(e) {
        e.target.classList.toggle('enabled');
        var music;
        // select current music depending on current screen
        if (this.started) {
          music = this.sound.music;
        } else {
          music = this.sound.intro;
        }
        // if music muted and game not paused, play music, otherwise mute music
        if (music.muted) {
          music.muted = false;
          if (!Game.viewport.classList.contains('game_paused') || music === this.sound.intro) {
            music.play();
          }
        } else {
          music.muted = true;
          music.pause();
        }
      };

      /**
       * Menu stats display handler method
       * @param {Object} e - event object
       */
      Game.prototype.menuStatsDisplayHandler = function(e) {
        e.target.classList.toggle('enabled');
        this.stats.element.style.display = this.stats.element.style.display === 'none' ? '' : 'none';
      };

      /**
       * Menu CRT effect handler method
       * @param {Object} e - event object
       */
      Game.prototype.menuCrtEffectHandler =  function(e) {
        e.target.classList.toggle('enabled');
        d.getElementById('screen').classList.toggle('crt');
      };

      /**
       * Init menu click event listeners
       */
      Game.prototype.initMenu = function() {
        d.getElementById('zoom_in').addEventListener('click', this.menuZoomInHandler, false);
        d.getElementById('zoom_out').addEventListener('click', this.menuZoomOutHandler, false);
        d.getElementById('music_play').addEventListener('click', this.menuMusicPlayHandler, false);
        d.getElementById('stats_display').addEventListener('click', this.menuStatsDisplayHandler, false);
        d.getElementById('crt_effect').addEventListener('click', this.menuCrtEffectHandler, false);
      };

      /**
       * Init info box to display data about the game and about the CV depending on current score
       */
      Game.prototype.initInfoBox = function() {
        this.infoBox = new InfoBox();
        this.infoBox.element.innerHTML = Data['score_0'].message; // get default game start message
        // if gamepad connected, display message with gamepad id, otherwise suggest to user to connect a gamepad
        if (this.gamepad && this.gamepad.connected && this.gamepad.id) {
          var shortId = this.gamepad.id;
          shortId = shortId.substring(0, shortId.lastIndexOf('('));
          this.infoBox.element.innerHTML += '<p>Ou utilisez votre manette ' + shortId + '.</p>';
        } else {
          this.infoBox.element.innerHTML += '<p>Ou encore mieux, branchez une manette de jeu compatible (ex.: PS3, PS4, XBox 360, XBox One).</p>';
        }
        Game.viewport.appendChild(this.infoBox.element);
      };

      /**
       * Init the stats (health points and score) displayed on top-right corner
       */
      Game.prototype.initStats = function() {
        this.stats = new Stats();
        // display heart pictures based on health points
        for (var i = 0; i < this.character.HP; i += 10) {
          var life = Utils.createElement('div', {'class': 'life'});
          this.stats.lifebar.appendChild(life);
        }
        Game.viewport.appendChild(this.stats.element);
      };

      /**
       * Init the character on the center of viewport
       */
      Game.prototype.initCharacter = function() {
        this.character = new Character();
        Game.viewport.appendChild(this.character.element);
        this.character.position = Utils.initPosition(this.character, this.character.element.offsetLeft, this.character.element.offsetTop);
      };

      /**
       * Init random number of enemies at random positions on viewport
       */
      Game.prototype.initEnemies = function() {
        Game.enemies = Utils.populateMapWithObjects(Enemy, Game.nbMinEnemies, Game.nbMaxEnemies, Game.viewport, 32, 32, '.item', 120);
        Game.enemiesCounter = Object.keys(Game.enemies).length; // keeps track of enemy count if we need to generate new ones
      };

      /**
       * Init random number of background items at random positions on viewport
       */
      Game.prototype.initBackgroundItems = function() {
        this.bgItems = Utils.populateMapWithObjects(BackgroundItem, Game.nbMinItems, Game.nbMaxItems, Game.viewport, 16, 16, '.item', 0);
      };

      /**
       * Init random number of trees at random positions on viewport
       */
      Game.prototype.initTrees = function() {
        this.trees = Utils.populateMapWithObjects(Tree, Game.nbMinTrees, Game.nbMaxTrees, Game.viewport, 86, 75, '.item', 160);
      };

      /**
       * Retrieve gamepad inputs and apply action depending on pressed buttons
       */
      Game.prototype.gamepadActions = function() {
        var btns = this.gamepad.buttons; // current gamepad buttons object
        var axes = this.gamepad.axes; // current gamepad analog sticks object

        if (!this.paused) {
          // move character depending on directional pad buttons or left analog stick pressed
          if ((btns[12] && btns[12].pressed && btns[15] && btns[15].pressed)
                || (axes[0] > 0.5 && axes[0] <= 1.0 && axes[1] >= -1.0 && axes[1] < -0.5)) {
            this.character.moveUpRight();
          } else if ((btns[12] && btns[12].pressed && btns[14] && btns[14].pressed)
                      || (axes[0] >= -1.0 && axes[0] < -0.5 && axes[1] >= -1.0 && axes[1] < -0.5)) {
            this.character.moveUpLeft();
          } else if ((btns[13] && btns[13].pressed && btns[15] && btns[15].pressed)
                      || (axes[0] > 0.5 && axes[0] <= 1.0 && axes[1] > 0.5 && axes[1] <= 1.0)) {
            this.character.moveDownRight();
          } else if ((btns[13] && btns[13].pressed && btns[14] && btns[14].pressed)
                      || (axes[0] >= -1.0 && axes[0] < -0.5 && axes[1] > 0.5 && axes[1] <= 1.0)) {
            this.character.moveDownLeft();
          } else if ((btns[12] && btns[12].pressed)
                      || (axes[0] > -0.5 && axes[0] <= 0.5 && axes[1] < -0.5)) {
            this.character.moveUp();
          } else if ((btns[15] && btns[15].pressed)
                      || (axes[0] > 0.5 && axes[1] > -0.5 && axes[1] <= 0.5)) {
            this.character.moveRight();
          } else if ((btns[13] && btns[13].pressed)
                      || (axes[0] > -0.5 && axes[0] <= 0.5 && axes[1] > 0.5)) {
            this.character.moveDown();
          } else if ((btns[14] && btns[14].pressed)
                      || (axes[0] < -0.5 && axes[1] > -0.5 && axes[1] <= 0.5)) {
            this.character.moveLeft();
          } else {
            this.character.stop();
          }

          // if "X" action button pressed (for PlayStation gamepad), shoot a fireball or open a chest if close enough
          if (btns[0] && btns[0].pressed) {
            if (!this.openChest()) {
              this.character.shoot(Game.timer);
            }
          }
        }

        // if "START" button pressed, either start the game if not started or toggle pause feature
        if (!this.buttonRepeat[9] && btns[9] && btns[9].pressed) {
          if (!this.started) {
            this.start();
          } else if (!this.paused) {
            this.pause();
          } else {
            this.resume();
          }
        }

        // keep track of repeated buttons
        for (var i = 0; i < btns.length; i++) {
          this.buttonRepeat[i] = btns[i].pressed;
        }
      };

      /**
       * Retrieve keyboard inputs and apply action depending on pressed keys
       */
      Game.prototype.keyActions = function() {
        // move character depending on key pressed (allowed keys are numeric keys between 0 and 9, and arrow keys)
        if (this.keyState['1'] || this.keyState['Numpad1'] || this.keyState[97]) {
          this.character.moveDownLeft();
        } else if (this.keyState['2'] || this.keyState['Numpad2'] || this.keyState['ArrowDown'] || this.keyState['Down'] || this.keyState[98] || this.keyState[40]) {
          this.character.moveDown();
        } else if (this.keyState['3'] || this.keyState['Numpad3'] || this.keyState[99]) {
          this.character.moveDownRight();
        } else if (this.keyState['4'] || this.keyState['Numpad4'] || this.keyState['ArrowLeft'] || this.keyState['Left'] || this.keyState[100] || this.keyState[37]) {
          this.character.moveLeft();
        } else if (this.keyState['6'] || this.keyState['Numpad6'] || this.keyState['ArrowRight'] || this.keyState['Right'] || this.keyState[102] || this.keyState[39]) {
          this.character.moveRight();
        } else if (this.keyState['7'] || this.keyState['Numpad7'] || this.keyState[103]) {
          this.character.moveUpLeft();
        } else if (this.keyState['8'] || this.keyState['Numpad8'] || this.keyState['ArrowUp'] || this.keyState['Up'] || this.keyState[104] || this.keyState[38]) {
          this.character.moveUp();
        } else if (this.keyState['9'] || this.keyState['Numpad9'] || this.keyState[105]) {
          this.character.moveUpRight();
        }

        // Spacebar key allows shooting a fireball or opening a chest if close enough
        if (this.keyState['Spacebar'] || this.keyState[' '] || this.keyState[32]) {
          if (!this.openChest()) {
            this.character.shoot(Game.timer);
          }
        }

        // Enter key allows toggling pause feature
        if (!this.keyRepeat && (this.keyState['Enter'] || this.keyState[13])) {
          this.pause();
        }
      };

      /**
       * Open a chest and reveal its content in info box if character is colliding with the chest
       */
      Game.prototype.openChest = function() {
        // check if the character is colliding with a chest
        var hasCollisions = Utils.hasCollisions(this.character.hitbox, d.querySelectorAll('.chest'), this.character.direction.x, this.character.direction.y);
        for (var id in hasCollisions.collidingIDs) {
          if (hasCollisions.collidingIDs[id] === true) {
            var chest = d.getElementById(id);
            // exit if chest is already opened
            if (chest.classList.contains('open')) {
              return true;
            }
            // retrieve message corresponding to chest id, play a sound, display the message in info box and pause the game
            var idTab = id.split('-');
            var artifactId = idTab[1];
            chest.classList.add('open');
            var sound = new Audio('audio/secret.wav');
            sound.play();
            this.infoBox.element.innerHTML = Data[artifactId].message;
            this.infoBox.open();
            this.paused = true;
            this.pauseAnimations();
            return true;
          }
        }
        return false;
      };

      /**
       * Pause the game (disable inputs, freeze animations and mute audio)
       */
      Game.prototype.pause = function() {
        this.paused = true;
        Game.viewport.classList.add('game_paused');
        this.pauseAnimations();
        this.sound.music.pause();
        if (this.character.sound.lowHP.currentTime > 0) {
          this.character.sound.lowHP.pause();
        }
      };

      /**
       * Freeze currently playing animations
       */
      Game.prototype.pauseAnimations = function() {
        var elements = Game.viewport.getElementsByTagName('div');
        for (var i = 0; i < elements.length; i++) {
          elements[i].style.animationPlayState = 'paused';
        }
      };

      /**
       * Resume the game (enable inputs, animations and audio)
       */
      Game.prototype.resume = function() {
        this.paused = false;
        this.infoBox.close();
        Game.viewport.classList.remove('game_paused');
        this.resumeAnimations();
        if (!this.sound.music.muted) {
          this.sound.music.play();
          if (this.character.sound.lowHP.currentTime > 0) {
            Utils.playAudioLoop(this.character.sound.lowHP);
          }
        }
      };

      /**
       * Resume currently playing animations
       */
      Game.prototype.resumeAnimations = function() {
        var elements = Game.viewport.getElementsByTagName('div');
        for (var i = 0; i < elements.length; i++) {
          elements[i].style.animationPlayState = 'running';
        }
      };

      /**
       * Game over sequence: pause the game, destroy event listeners, destroy game loop, stop audio,
       * display black screen, restart the game after 2 seconds delay
       * @param {Object} character
       */
      Game.prototype.gameOver = function(character) {
        this.paused = true;
        this.pauseAnimations();
        d.removeEventListener('keydown', this.keydownHandler, false);
        d.removeEventListener('keyup', this.keyupHandler, false);
        w.removeEventListener('gamepadconnected', this.gamepadConnectedHandler, false);
        w.removeEventListener('gamepaddisconnected', this.gamepadDisconnectedHandler, false);
        d.getElementById('zoom_in').removeEventListener('click', this.menuZoomInHandler, false);
        d.getElementById('zoom_out').removeEventListener('click', this.menuZoomOutHandler, false);
        d.getElementById('music_play').removeEventListener('click', this.menuMusicPlayHandler, false);
        d.getElementById('stats_display').removeEventListener('click', this.menuStatsDisplayHandler, false);
        d.getElementById('crt_effect').removeEventListener('click', this.menuCrtEffectHandler, false);
        w.cancelAnimFrame(this.rAF);
        delete this.rAF;
        this.sound.music.pause();
        character.sound.lowHP.pause();
        this.sound.gameover.play();
        Game.viewport.classList.add('fadeout');
        setTimeout(function() {
          Game.viewport.classList.add('game_over');
          Game.viewport.classList.remove('fadeout');
          Game.viewport.innerHTML = '';
          Game.run();
        }, 2000);
      };
 
      /***********************************************************************/
      /*                          BackgroundItem module                      */
      /* @module BackgroundItem                                              */
      /* @desc Contains the logic for BackgroundItem creation and management */
      /***********************************************************************/
      var BackgroundItem = (function() {
        /**
         * Available background items classes
         */
        var items = ['small_flower_yellow', 'medium_flower_yellow', 'big_flower_yellow', 'big_flower_orange', 'medium_flower_pink', 'grey_stone', 'brown_stone'];

        /**
         * BackgroundItem object
         * @constructor
         * @this {BackgroundItem}
         * @param {number} index - index of the created object to pass to the element id
         */
        function BackgroundItem(index) {
          return this.init(index);
        }

        /**
         * Init BackgroundItem object
         * @param {number} index - index of the created object to pass to the element id
         */
        BackgroundItem.prototype.init = function(index) {
          this.element = Utils.createElement('div', {
            'id':    'bgItem_' + index,
            'class': items[Math.floor(Math.random() * items.length)] + ' item' // pick item class randomly in items list
          });
        };

        return BackgroundItem;
      }());

      /*************************************************************/
      /*                          Tree module                      */
      /* @module Tree                                              */
      /* @desc Contains the logic for Tree creation and management */
      /*************************************************************/
      var Tree = (function() {
        /**
         * Available tree types classes
         */
        var types = ['tree_branch_green', 'tree_branch_brown', 'tree_branch_pink'];

        /**
         * Tree object
         * @constructor
         * @this {Tree}
         * @param {number} index - index of the created object to pass to the element id
         */
        function Tree(index) {
          return this.init(index);
        }

        /**
         * Init Tree object
         * @param {number} index - index of the created object to pass to the element id
         */
        Tree.prototype.init = function(index) {
          var branch = Utils.createElement('div', { 'class': types[Math.floor(Math.random() * types.length)] });// pick tree type class randomly in tree types list
          var trunk = Utils.createElement('div', { 'class': 'tree_trunk solid item' });
          this.element = Utils.createElement('div', { 'id': 'tree_' + index, 'class': 'tree' });
          this.element.appendChild(branch);
          this.element.appendChild(trunk);
        };

        return Tree;
      }());

      /**************************************************************/
      /*                          Chest module                      */
      /* @module Chest                                              */
      /* @desc Contains the logic for Chest creation and management */
      /**************************************************************/
      var Chest = (function() {
        /**
         * Chest object
         * @constructor
         * @this {Chest}
         * @param {number} artifactId - id of the related artifact in Data
         */
        function Chest(artifactId) {
          return this.init(artifactId);
        }

        /**
         * Init Chest object
         * @param {number} artifactId - id of the related artifact in Data
         */
        Chest.prototype.init = function(artifactId) {
          this.element = Utils.createElement('div', { 'id': 'chest-' + artifactId, 'class': 'chest solid' });
        };

        return Chest;
      }());

      /****************************************************************/
      /*                          InfoBox module                      */
      /* @module InfoBox                                              */
      /* @desc Contains the logic for InfoBox creation and management */
      /****************************************************************/
      var InfoBox = (function() {
        /**
         * InfoBox object
         * @constructor
         * @this {InfoBox}
         */
        function InfoBox() {
          return this.init();
        }

        /**
         * Init InfoBox object
         */
        InfoBox.prototype.init = function() {
          this.element = Utils.createElement('div', { 'id': 'info', 'class': 'box' });
        };

        /**
         * Open info box (toggle visibility)
         */
        InfoBox.prototype.open = function() {
          this.element.className = 'box visible';
        };

        /**
         * Close info box (toggle visibility)
         */
        InfoBox.prototype.close = function() {
          this.element.className = 'box hidden';
        };

        return InfoBox;
      }());


      /**************************************************************/
      /*                          Stats module                      */
      /* @module Stats                                              */
      /* @desc Contains the logic for Stats creation and management */
      /**************************************************************/
      var Stats = (function() {
        /**
         * Stats object
         * @constructor
         * @this {Stats}
         */
        function Stats() {
          return this.init();
        }

        /**
         * Init Stats object
         */
        Stats.prototype.init = function() {
          this.element = Utils.createElement('div', { 'id': 'stats' });
          this.initLifeBar();
          this.initScore();
        };

        /**
         * Init life bar object
         */
        Stats.prototype.initLifeBar = function() {
          this.lifebar = Utils.createElement('div', { 'id': 'lifebar' });
          this.element.appendChild(this.lifebar);
        };

        /**
         * Init score object
         */
        Stats.prototype.initScore = function() {
          Stats.score = 0;
          this.score = Utils.createElement('div', { 'id': 'score', 'class': 'box' });
          this.score.innerHTML = Stats.score;
          this.element.appendChild(this.score);
        };

        /**
         * Draw current health points and score
         * @param {number} hp - current character health points
         */
        Stats.prototype.draw = function(hp) {
          var lifePoints = d.querySelectorAll('.life');
          for (var i = lifePoints.length - 1; i > hp / 10 - 1; i--) {
            lifePoints[i].style.visibility = 'hidden';
          }
          this.score.innerHTML = Stats.score;
        };

        return Stats;
      }());

      /**************************************************************/
      /*                          Enemy module                      */
      /* @module Enemy                                              */
      /* @desc Contains the logic for Enemy creation and management */
      /**************************************************************/
      var Enemy = (function() {
        /**
         * Enemy object
         * @constructor
         * @this {Enemy}
         */
        function Enemy(index) {
          return this.init(index);
        }

        /**
         * Init Enemy object
         * @param {number} index - index of the created object to pass to the element id
         */
        Enemy.prototype.init = function(index) {
          this.chase = {
            radius: 140, // radius distance from which enemy starts chasing the character
            speed:  0.7 // enemy has an increased speed when chasing the character
          };
          this.attack = {
            radius: 60, // radius distance from which enemy adopts an aggressive stance
            damage: 10, // inflicted damage to character HP
            delay:  1 // delay between two attacks in seconds
          };
          this.burn = {
            delay: 2 // burn animation delay in seconds
          };
          this.die = {
            status: false,
            delay:  0.8 // die animation delay in seconds
          };
          this.HP = 10; // enemy health points
          this.points = 5; // enemy game points reward
          this.sound = {
            hit: new Audio('audio/enemy_hit.wav'),
            die: new Audio('audio/enemy_die.wav')
          };
          this.element = Utils.createElement('div', { 'id': 'enemy_' + index, 'class': 'enemy solid item' });
        };

        /**
         * Init a random move for current enemy
         */
        Enemy.prototype.initRandomMove = function() {
          if (!this.motion || ((Game.timer - this.motion.start) / 1000 > this.motion.duration)) {
            this.motion = {
              start:    Game.timer, // motion start time 
              x:        Utils.getRandomInt(-1, 1), // motion horizontal direction: left (-1), stand still (0), right (1)
              y:        Utils.getRandomInt(-1, 1), // motion vertical direction: top (-1), stand still (0), bottom (1)
              duration: Utils.getRandomInt(3, 7), // motion duration in seconds
              speed:    Utils.getRandomInt(2, 5) / 10 // motion speed in pixel/frame
            };
            this.moveX = this.motion.speed * this.motion.x; // enemy horizontal move
            this.moveY = this.motion.speed * this.motion.y; // enemy vertical move
          }
        };

        /**
         * Invert the current move of the current enemy when hitting bounds
         */
        Enemy.prototype.invertMove = function() {
          if (this.motion) {
            this.moveX *= -this.motion.x; // invert enemy horizontal move
            this.moveY *= -this.motion.y; // invert enemy vertical move
          }
        };

        /**
         * Define the enemy behavior (random move, chasing, or attacking) depending on distance from the character
         * @param {Object} character
         */
        Enemy.prototype.initEnemyBehavior = function(character) {
          var distance = Utils.getDistance(character.element, this.element); // get distance between enemy and character
          // If enemy is close enough from character, it stops current motion and starts chasing the character,
          // and when it's even closer, it displays attack animation. If distance is beyond the chase limit, a new random motion is initiated.
          if (distance <= this.chase.radius) {
            delete this.motion;
            this.moveX = this.chase.speed * ((character.position.x - this.position.x) / distance);
            this.moveY = this.chase.speed * ((character.position.y - this.position.y) / distance);
            if (distance <= this.attack.radius) {
              this.element.classList.add('attack');
            } else {
              this.element.classList.remove('attack');
              this.element.classList.remove('new');
            }
          } else {
            this.initRandomMove();
          }
        };
        
        /**
         * Move the enemy across the viewport depending on position and collisions
         * @param {Object} character
         */
        Enemy.prototype.move = function(character) {
          this.initEnemyBehavior(character);

          // if enemy reaches the game bounds, its motion is inverted, otherwise enemy moves and has interactions with the character
          if (!Utils.hasInnerBoxCollision(this.element, Game.viewport, this.moveX, this.moveY)) {
            this.element.classList.remove('anim_paused');
            this.attacking(character);
            this.hit();
            this.dying();
          } else {
            this.invertMove();
          }

          // change enemy animation depending on move direction
          if (this.moveX === 0 && this.moveY === 0) {
            this.stopMove();
          } else if (this.moveX > 0) {
            this.rightMove();
          } else {
            this.leftMove();
          }
        };

        /**
         * Perform an attack every N seconds when an enemy is colliding with the character
         * @param {Object} character 
         */
        Enemy.prototype.attacking = function(character) {
          // init attack start if enemy is colliding with the character
          if (!this.attack.start) {
            var collisions = Utils.hasCollisions(this.element, d.querySelectorAll('.solid'), this.moveX, this.moveY);
            this.position.x += !collisions.x ? this.moveX : 0;
            this.position.y += !collisions.y ? this.moveY : 0;
            if (collisions.collidingIDs['character_hitbox']) {
              this.attack.start = Game.timer;
            }
          }
          
          // perform attack after a given delay while the character is alive
          if (character.HP > 0 && this.attack.start && ((Game.timer - this.attack.start) / 1000) > this.attack.delay) {
            // apply damages to character and play sound and animation
            character.HP -= this.attack.damage;
            character.sound.hurt.play();
            character.element.style.filter = 'sepia() saturate(500%) hue-rotate(310deg)';
            setTimeout(function() { character.element.style.filter = 'none'; }, 100);
            // if enemy HP runs below 20%, play a warning sound
            if (character.HP <= character.maxHP * 0.2) {
              Utils.playAudioLoop(character.sound.lowHP);
            }
            delete this.attack.start;
          }
        };

        /**
         * Perform HP decrease and burn animation when an enemy is hit by a fireball
         */
        Enemy.prototype.hit = function() {
          var hits = Utils.hasCollisions(this.element, d.querySelectorAll('.fireball'), this.moveX, this.moveY);
          // if enemy colliding with fireball, decrease enemy HP, play sound and display burn animation on enemy
          if (hits.x || hits.y) {
            this.HP -= Fireball.damage;
            this.sound.hit.play();
            this.burn.start = Game.timer;
            if (!this.burn.element) {
              this.burn.element = Utils.createElement('div', { 'class': 'burn' });
              this.element.appendChild(this.burn.element);
            }
          }
          if (this.burn.start && this.burn.element && ((Game.timer - this.burn.start) / 1000) > this.burn.delay) {
            this.element.removeChild(this.burn.element);
            delete this.burn.element;
            delete this.burn.start;
          }
        };

        /**
         * Perform animations when an enemy is dying and check whether we need to generate new enemies
         */
        Enemy.prototype.dying = function() {
          // if enemy HP are equal to 0, play sound, start dying animation and give points reward to the player
          if (!this.die.status && this.HP <= 0) {
            this.die.status = true;
            Stats.score += this.points;
            this.element.classList.add('die');
            this.sound.die.play();
            this.die.start = Game.timer;
          }
          // if score is met, drop a treasure chest
          if (this.die.start && ((Game.timer - this.die.start) / 1000) > this.die.delay) {
            this.dropChest('score_' + Stats.score);
            this.element.parentNode.removeChild(this.element);
            delete Game.enemies[this.element.id];
            this.reborn();
          }
        };

        /**
         * Generate new enemies on map if enemy count reaches min limit
         */
        Enemy.prototype.reborn = function() {
          if (Object.keys(Game.enemies).length <= Game.nbMinEnemies) {
            var delta = Game.nbMaxEnemies - Game.nbMinEnemies;
            var newEnemies = Utils.populateMapWithObjects(Enemy, 0, delta, Game.viewport, 32, 32, '.item', 120, Game.enemiesCounter);
            Game.enemies = Utils.mergeObjects(Game.enemies, newEnemies); // merge old enemies with new ones
            Game.enemiesCounter += Object.keys(newEnemies).length; // keep track of enemy counter
          }
        };

        /**
         * Drop chest on the floor after killing enemy, depending on current score
         * @param {number} artifactId - id of the related artifact in Data
         */
        Enemy.prototype.dropChest = function(artifactId) {
          if (!(artifactId in Data)) {
            return;
          }
          var chest = new Chest(artifactId);
          this.element.parentNode.appendChild(chest.element);
          chest.position = Utils.initPosition(chest, this.position.x, this.position.y);
        };

        /**
         * Stop enemy animation
         */
        Enemy.prototype.stopMove = function() {
          this.element.classList.remove('right');
          this.element.classList.remove('left');
        };

        /**
         * Left move enemy animation
         */
        Enemy.prototype.leftMove = function() {
          this.element.classList.add('left');
          this.element.classList.remove('right');
        };

        /**
         * Right move enemy animation
         */
        Enemy.prototype.rightMove = function() {
          this.element.classList.remove('left');
          this.element.classList.add('right');
        };

        return Enemy;
      }());

      /******************************************************************/
      /*                          Character module                      */
      /* @module Character                                              */
      /* @desc Contains the logic for Character creation and management */
      /******************************************************************/
      var Character = (function() {
        Character.fireballs = [];

        /**
         * Character object
         * @constructor
         * @this {Character}
         */
        function Character() {
          return this.init();
        }

        /**
         * Init Character object
         */
        Character.prototype.init = function() {
          this.speed = 1; // character speed in pixel/frame
          this.maxHP = 100; // character maximum health points
          this.HP = this.maxHP; // character current health points
          this.direction = { name: 'down', x: 0, y: 1 }; // character direction
          this.sound = {
            hurt:  new Audio('audio/character_hurt.wav'),
            lowHP: new Audio('audio/low_hp.wav')
          };
          this.hitbox = Utils.createElement('div', { 'id': 'character_hitbox', 'class': 'solid item' }); // character hitbox to check collision against
          this.element = Utils.createElement('div', { 'id': 'character' });
          this.element.appendChild(this.hitbox);
        };

        /**
         * Move the character across the viewport depending on position and collisions
         */
        Character.prototype.move = function() {
          if (this.oldDirection) {
            this.element.classList.remove(this.oldDirection.name);
          }
          this.element.classList.add(this.direction.name);
          this.oldDirection = this.direction; // keep track of previous direction
          // move the character across the screen while it's not colliding with the viewport bounds, otherwise stop the character
          if (!Utils.hasInnerBoxCollision(this.element, Game.viewport, this.direction.x, this.direction.y)) {
            this.element.classList.remove('anim_paused');
            var hasCollisions = Utils.hasCollisions(this.hitbox, d.querySelectorAll('.solid'), this.direction.x, this.direction.y);
            this.position.x += !hasCollisions.x ? this.direction.x * this.speed : 0;
            this.position.y += !hasCollisions.y ? this.direction.y * this.speed : 0;
          } else {
            this.stop();
          }
        };

        /**
         * Move the character to down-left direction
         */
        Character.prototype.moveDownLeft = function() {
          this.direction = { name: 'down_left', x: -1, y: 1 };
          this.move();
        };

        /**
         * Move the character to down direction
         */
        Character.prototype.moveDown = function() {
          this.direction = { name: 'down', x: 0, y: 1 };
          this.move();
        };

        /**
         * Move the character to down-right direction
         */
        Character.prototype.moveDownRight = function() {
          this.direction = { name: 'down_right', x: 1, y: 1 };
          this.move();
        };

        /**
         * Move the character to left direction
         */
        Character.prototype.moveLeft = function() {
          this.direction = { name: 'left', x: -1, y: 0 };
          this.move();
        };

        /**
         * Move the character to right direction
         */
        Character.prototype.moveRight = function() {
          this.direction = { name: 'right', x: 1, y: 0 };
          this.move();
        };

        /**
         * Move the character to up-left direction
         */
        Character.prototype.moveUpLeft = function() {
          this.direction = { name: 'up_left', x: -1, y: -1 };
          this.move();
        };

        /**
         * Move the character to up direction
         */
        Character.prototype.moveUp = function() {
          this.direction = { name: 'up', x: 0, y: -1 };
          this.move();
        };

        /**
         * Move the character to up-right direction
         */
        Character.prototype.moveUpRight = function() {
          this.direction = { name: 'up_right', x: 1, y: -1 };
          this.move();
        };

        /**
         * Stop the character animation
         */
        Character.prototype.stop = function() {
          this.element.classList.add('anim_paused');
        };

        /**
         * Make the character shoot fireballs
         * @param {DOMHighResTimeStamp} timer - current game timer
         */
        Character.prototype.shoot = function(timer) {
          // shoot a fireball if character has not reach fireball shoot limit and delay between two shots is met
          if (Character.fireballs.length === 0
            || Character.fireballs.length > 0
              && Character.fireballs.length < Fireball.limit
              && (timer - Character.fireballs[Character.fireballs.length - 1].start) / 1000 > Fireball.delay) {
            var fireball = new Fireball(timer, this.direction);
            this.element.parentNode.appendChild(fireball.element);
            // fireball initial position is related to character position
            fireball.position = Utils.initPosition(fireball, this.position.x + fireball.element.offsetLeft, this.position.y + fireball.element.offsetTop);
            fireball.sound.play();
            Character.fireballs.push(fireball);
          }
        };

        return Character;
      }());

      /*****************************************************************/
      /*                          Fireball module                      */
      /* @module Fireball                                              */
      /* @desc Contains the logic for Fireball creation and management */
      /*****************************************************************/
      var Fireball = (function() {
        Fireball.limit = 5; // number of shots allowed simultaneously
        Fireball.delay = 0.8; // delay between two shots in seconds
        Fireball.damage = 5; // inflicted damage to enemy HP

        /**
         * Fireball object
         * @constructor
         * @this {Fireball}
         */
        function Fireball(index, direction) {
          return this.init(index, direction);
        }

        /**
         * Init Fireball object
         * @param {number} index - index of the created object to pass to the element id
         * @param {Object} direction - the fireball direction depending on character direction
         */
        Fireball.prototype.init = function(index, direction) {
          this.speed = 2; // shot speed in pixel/frame
          this.start = Game.timer; // shot start
          this.duration = 2; // shot duration in seconds
          this.direction = direction; // shot direction
          this.sound = new Audio('audio/fireshot.wav');
          this.element = Utils.createElement('div', { 'id': 'fireball_' + index, 'class': 'fireball ' + this.direction.name });
        };

        /**
         * Move the fireball across the viewport depending on position and collisions
         */
        Fireball.prototype.move = function() {
          // if fireball delay is met, remove firebal, otherwise move fireball across the screen while it's not colliding with viewport bounds or other items
          if ((Game.timer - this.start) / 1000 > this.duration) {
            this.stop();
          } else if (!Utils.hasInnerBoxCollision(this.element, Game.viewport, this.direction.x, this.direction.y)) {
            var collisions = Utils.hasCollisions(this.element, d.querySelectorAll('.solid:not(#character_hitbox)'), this.direction.x, this.direction.y);
            if (collisions.x || collisions.y) {
              this.stop();
            } else {
              this.position.x += this.direction.x * this.speed;
              this.position.y += this.direction.y * this.speed;
            }
          } else {
            this.stop();
          }
        };

        /**
         * Stop and remove fireball
         */
        Fireball.prototype.stop = function() {
          this.element.parentNode.removeChild(this.element);
          var index = Character.fireballs.indexOf(this);
          if (index > -1) {
            Character.fireballs.splice(index, 1);
          }
        };

        return Fireball;
      }());

      return Game;
    }(w, d, Utils || {}, Data || {}));

  /**
   * Set event listener to check if the DOM has finished loading
   * and to prevent display on mobile devices. If all is ok, run the game.
   */
  d.addEventListener('DOMContentLoaded', function() {
    if (Utils.isMobileDevice()) {
      d.getElementById('loading-container').style.display = 'none';
      d.getElementsByTagName('body')[0].style.background = 'none';
      d.getElementById('game').style.display = 'none';
      d.getElementById('gitlab').style.display = 'none';
      d.getElementById('mobile-notice').style.display = 'flex';
    } else {
      Game.run();
    }
  });
}(window, document));